package com.example.demo.controller;

import com.example.demo.service.ScreenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Cauli
 * @date 2022/12/1 9:40
 * @description 控制层
 */
@RestController
@RequestMapping("/screen")
public class ScreenController {
    @Autowired
    private ScreenService screenService;

    @GetMapping(value = "/export")
    public void export(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        screenService.export(httpServletRequest, httpServletResponse);
    }
}
