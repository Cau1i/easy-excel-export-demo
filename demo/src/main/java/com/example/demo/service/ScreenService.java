package com.example.demo.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Cauli
 * @date 2022/12/1 9:47
 * @description 业务层
 */
public interface ScreenService {
    /**
     * 导出
     *
     * @param httpServletRequest
     * @param httpServletResponse
     */
    void export(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse);
}
